function changeImage(object, img) {
    object.src = img;
    if (object.width == 400) {
        object.width = 900;
        object.height = 900;
    } else {
        object.width = 400;
        object.height = 400;
    }
}

function showCar(object, carImage) {
    var childs = document.getElementById("carList").children;
    for (var i = 0; i < childs.length; i++) {
        childs[i].style.color = "black";
    }
    object.style.color = "red";
    document.getElementById("carContainer").style.backgroundImage = 'url("' + carImage + '")';
}

function changeFontSize() {
    var childs = document.getElementById("carList").children;
    for (var i = 0; i < childs.length; i ++) {
        if (childs[i].style.fontSize == "18px") {
            childs[i].style.fontSize = "32px";
        } else {
            childs[i].style.fontSize = "18px";
        }
    }
}

function addParragraph() {
    var container = document.getElementById("emptyDiv");
    var node = document.createElement("p");
    node.innerHTML = new Date().toLocaleString();
    container.appendChild(node);
}